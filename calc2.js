function getNumber() {
  return parseInt(prompt("Please enter the number"));
}
function getOperator() {
  return prompt("Please choose the operator", "+, -, /, *");
}

function calculate(callBack1, callBack2) {
  let num1 = callBack1();
  let num2 = callBack1();
  let operator = callBack2();
  if (isNaN(num1)) {
    alert("The first number is not valid. Please try again")
    return
  }
  if (isNaN(num2)) {
    alert("The second number is not valid. Please try again")
    return
  }
  if (operator === "/" && num2 === 0) {
        alert("You cannot divide by 0")
    return;
  }

  switch (operator) {
    case "+":
      document.write(num1 + num2);
      break;
    case "-":
      document.write(num1 - num2);
      break;
    case "/":
      document.write(num1 / num2);
      break;
    case "*":
      document.write(num1 * num2);
      break;
    default:
      document.write("Please choose correct operator");
  }

}

calculate(getNumber, getOperator);

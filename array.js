// Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.

function adding(array) {
  let sum = 0;
  for (let i = 0; i < array.length; i++){
    sum += array[i];
  }
  return sum;
}
function map(fn, array) {
  return fn(array);
}
document.write(map(adding, [1, 3, 5]));